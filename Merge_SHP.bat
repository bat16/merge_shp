@ECHO OFF
setlocal enabledelayedexpansion
ECHO TIF Creator from BIL files in folder
ECHO Author: Szczepkowski Marek
ECHO Date: 25-06-2019
ECHO Company: Visimind Ltd
ECHO Version: 1.0
ECHO.

SET QGIS_ROOT="C:\Program Files\QGIS 2.14"
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET WORK=%cd%

REM COUNTER FILES
dir /b *.shp 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

ECHO MERGING FILES IN DIRECTORY...
FOR /F %%i IN ('dir /b "%WORK%\*.shp"') do (
  ECHO 	ADDING %%i    !Counter! / %count% FILES
  ogr2ogr -update -append -f "ESRI Shapefile" merged.shp %%i 
  set /A Counter+=1	
)
ECHO. 
ECHO MERGING COMPLETE
pause